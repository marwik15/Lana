
#define STB_TRUETYPE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION

#include <lana/window.hpp>


namespace lana {

	void Window::create() {
		native_create();
	}

	void Window::loop(){
		native_loop();
	}

	void Window::show(bool visible){
		native_show(visible);
	}

	void Window::name(std::wstring name){
		native_window_name(name);
	}

	void Window::name(std::string name){
		std::wstring widestr = std::wstring(name.begin(), name.end());
		native_window_name(widestr);
	}

	void Window::size(int x, int y){
		native_size(x, y);
	}

	lana::Size Window::size(){
		return native_size();
	}

	void Window::pos(int x, int y){
		native_pos(x, y);
	}

	lana::Size Window::pos(){
		return native_size();
	}
}