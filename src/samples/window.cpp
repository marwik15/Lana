﻿#include <lana/window.hpp>

int main() {

	lana::Window window;
	
	window.create();
	window.name(L"Lana Window test");
	window.show(true);
	window.pos(400, 100);
	window.size(800, 600);

	window.loop();

}