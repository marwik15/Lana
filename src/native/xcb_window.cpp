#ifdef linux

#include <lana/native/xcb_window.hpp>

namespace lana {

    xcbWindow::xcbWindow() {}

	int xcbWindow::native_create(){
        
        bd.CreateBuffer(400, 400);
        bd.ClearBuffer(PixelColor(100, 100, 100));
        bd.Drawline(0, 0, 100, 100, PixelColor(255, 0, 0, 255));
        bd.DrawRectangle(0, 0, 100, 100);
        bd.FillArea(100, 100, 200, 200, PixelColor(87, 44, 32, 100));
        bd.DrawText(10,10,L"reee");

        // open connection with the server 
        c = xcb_connect(NULL, NULL);

        if (!c) {
            printf("Cannot open display\n");
            exit(1);
        }

        s = xcb_setup_roots_iterator(xcb_get_setup(c)).data;

        // create image 
        image = CreateTrueColorImage(c, 400, 400, bd.GetBuffer());
        if (image == NULL) {
            printf("Cannot create image\n");
            xcb_disconnect(c);
        }
        image32 = image->data;

        // create window 
        mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
        values[0] = s->white_pixel;
        values[1] = XCB_EVENT_MASK_EXPOSURE |
            XCB_EVENT_MASK_KEY_PRESS |
            XCB_EVENT_MASK_BUTTON_PRESS;

        w = xcb_generate_id(c);
        xcb_create_window(c, XCB_COPY_FROM_PARENT, w, s->root,
            10, 10, image->width, image->height, 1,
            XCB_WINDOW_CLASS_INPUT_OUTPUT,
            s->root_visual,
            mask, values);

        native_window_name(L"Lana Linux Window");
        native_show(true);

        // create backing pixmap 
        pmap = xcb_generate_id(c);
        xcb_create_pixmap(c, 24, pmap, w, image->width, image->height);

        // create pixmap plot gc 
        mask = XCB_GC_FOREGROUND | XCB_GC_BACKGROUND;
        values[0] = s->black_pixel;
        values[1] = 0xffffff;

        gc = xcb_generate_id(c);
        xcb_create_gc(c, gc, pmap, mask, values);

        // put the image into the pixmap 
        xcb_image_put(c, pmap, gc, image, 0, 0, 0);
      
        return 1;
	}

    void xcbWindow::native_window_name(std::wstring name){
        //setup converter
        using convert_type = std::codecvt_utf8<wchar_t>;
        std::wstring_convert<convert_type, wchar_t> converter;

        title = converter.to_bytes(name);

        xcb_change_property(c, XCB_PROP_MODE_REPLACE, w, XCB_ATOM_WM_NAME, XCB_ATOM_STRING, 8, title.length(), title.c_str());
        
        //flush the request 
        xcb_flush(c);
    }

    void xcbWindow::native_show(bool show){
        if(show){
            xcb_map_window(c, w);
        }
        else {
            xcb_unmap_window(c, w);
        }
        xcb_flush(c);
    }
    void xcbWindow::native_size(int width, int height) {
        const static uint32_t values[] = { width, height };
        xcb_configure_window(c, w, XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT, values);
    }

    void xcbWindow::native_pos(int x, int y) {
        const static uint32_t values[] = { x, y };
        xcb_configure_window(c, w, XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y, values);
    }

    xcb_format_t* xcbWindow::find_format(xcb_connection_t* c, uint8_t depth, uint8_t bpp){
        const xcb_setup_t* setup = xcb_get_setup(c);
        xcb_format_t* fmt = xcb_setup_pixmap_formats(setup);
        xcb_format_t* fmtend = fmt + xcb_setup_pixmap_formats_length(setup);
        for (; fmt != fmtend; ++fmt)
            if ((fmt->depth == depth) && (fmt->bits_per_pixel == bpp)) {
                return fmt;
            }
        return 0;
    }

    xcb_image_t* xcbWindow::CreateTrueColorImage(xcb_connection_t* c, int width, int height, void* data){

        const xcb_setup_t* setup = xcb_get_setup(c);
        unsigned char* image32 = (unsigned char*)malloc(width * height * 4);
        xcb_format_t* fmt = find_format(c, 24, 32);
        if (fmt == NULL)
            return NULL;

        return xcb_image_create(width,
            height,
            XCB_IMAGE_FORMAT_Z_PIXMAP,
            fmt->scanline_pad,
            fmt->depth,
            fmt->bits_per_pixel,
            0,
            (xcb_image_order_t)setup->image_byte_order,
            XCB_IMAGE_ORDER_LSB_FIRST,
            data,
            width * height * 4,
            (unsigned char*)data);
    }

    void xcbWindow::native_loop() {
        // event loop 
        while (!done && (e = xcb_wait_for_event(c))) {
            switch (e->response_type) {
            case XCB_EXPOSE:
                ee = (xcb_expose_event_t*)e;

                xcb_copy_area(c, pmap, w, gc,
                    ee->x,
                    ee->y,
                    ee->x,
                    ee->y,
                    ee->width,
                    ee->height);
                xcb_flush(c);
                image32 += 16;
                break;

            case XCB_KEY_PRESS:
                done = 1;
                break;

            case XCB_BUTTON_PRESS:
                bd.FillArea(100, 100, 200, 200, PixelColor(255, 1, 199, 100));
                xcb_image_put(c, pmap, gc, image, 0, 0, 0);
                xcb_copy_area(c, pmap, w, gc, 0, 0, 0, 0, image->width, image->height);
                xcb_flush(c);
                break;
            }
            free(e);
        }

        // free pixmap 
        xcb_free_pixmap(c, pmap);

        // close connection to server 
        xcb_disconnect(c);
    }
} //namespace lana

#endif //linux