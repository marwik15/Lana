﻿#ifdef _WIN32

#define STB_TRUETYPE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION

#include <lana/native/win32_window.hpp>
#include <bufferDrawer/bufferDrawer.hpp>

namespace lana {

    win32Window::win32Window() {

    }

    LRESULT CALLBACK win32Window::WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
        HDC          hdc;
        PAINTSTRUCT  ps;

        switch (uMsg) {
        case WM_DESTROY:
            PostQuitMessage(0);
            return 0;

        case WM_PAINT: {
            PAINTSTRUCT ps;
            hdc = BeginPaint(hwnd, &ps);
            FillRect(hdc, &ps.rcPaint, (HBRUSH)(15));
            OnPaint(hdc);
            EndPaint(hwnd, &ps);
            return 0;
        }
        return 0;
        }

        return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }

    void win32Window::OnPaint(HDC hdc){
        BufferDrawer bd(400,400,PixelColor(0, 0, 255,255));

        bd.Drawline(0, 0, 100, 100, PixelColor(255, 0, 0,255));
        bd.DrawRectangle(0, 0, 100, 100);
        bd.FillArea(100, 100, 200, 200,PixelColor(87, 44, 32,100));
        bd.DrawText(0, 0, L"reeee");

        Graphics graphics(hdc);
        Bitmap bmp(400, 400, bd.getWidth() * 4, PixelFormat32bppARGB, (BYTE*)bd.GetBuffer());
       
        graphics.DrawImage(&bmp,0,0);

    }

} //namespace lana

#endif //WIN32