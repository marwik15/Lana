﻿#pragma once
#include <math.h>
#include <iostream>
#include <string>
#include <cstring> //memcpy
#include <stb/stb_truetype.h>
#include <stb/stb_image_write.h>

#include <cmrc/cmrc.hpp>
CMRC_DECLARE(LanaResources);

struct PixelColor {
    uint8_t b,g,r, w; // GDI+ PixelFormat32bppARGB and X bitmap
    
	PixelColor() {
        r = g = b = w = 255;
	}

	PixelColor(int r, int g, int b, int w) : r(r), g(g),b(b),w(w) {}
    PixelColor(int r, int g, int b) : r(r), g(g), b(b), w(255) {}
};

class BufferDrawer {
private:
    struct fontInfo {
        fontInfo(std::string name, unsigned char* fontBuffer, long fontBufferSize) 
            : name (name), fontBuffer(fontBuffer), fontBufferSize(fontBufferSize) {}
        std::string name;
        unsigned char* fontBuffer;
        long fontBufferSize;
    };

    int width, height;

    std::vector<fontInfo> fonts;

    PixelColor* Buffer;

    void sizeGuard(int& x,int& y) {
        if (x > width-1) x = width-1;
        if (y > height-1) y = height-1;
    }

public:
    int getWidth() {
        return width;
    }

    BufferDrawer() {}

    BufferDrawer(int width,int height) {
        CreateBuffer(width, height);
        ClearBuffer(PixelColor());
    }

    BufferDrawer(int width, int height, PixelColor backgroundColor) {
        CreateBuffer(width, height);
        ClearBuffer(backgroundColor);
    }

    ~BufferDrawer() {
        DeleteBuffer();

        //delete loaded fonts
        for (auto& f : fonts) {
            delete f.fontBuffer;
        }
    }

    void CreateBuffer(int w,int h) {
	    width = w; height = h;
	    Buffer = new PixelColor[width * height * 4];
    }

    void PutPixelColor(int x, int y, PixelColor p = PixelColor(255, 255, 255, 255)) {
        sizeGuard(x,y);
	    Buffer[y * width + x] = p;
    }

    void ClearBuffer(PixelColor p = PixelColor()) {
	    for (int y = 0; y < height; y++) {
		    for (int x = 0; x < width; x++) {
			    PutPixelColor(x,y, p);
		    }
	    }
    }

    PixelColor* GetBuffer() {
	    return Buffer;
    }

    void SaveBufferPNG(std::string path = "buffer.png") {
        stbi_write_png(path.c_str(), width, height, 4, Buffer, width * 4);
    }

    void SaveBufferBMP(std::string path = "buffer.bmp") {
        stbi_write_bmp(path.c_str(), width, height, 4, Buffer);
    }

    void SaveBufferJPG(std::string path = "buffer.bmp",int quality = 100) {
        stbi_write_jpg(path.c_str(), width, height, 4, Buffer, quality);
    }

    void FillArea(int x, int y, int width, int height, PixelColor p = PixelColor()) {

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                PutPixelColor(x + j, y + i, p);
            }
        }
    }

    void Drawline(const int x1, const int y1, const int x2, const int y2, PixelColor p = PixelColor()) {

        float  x, y,dx, dy, step;
        int i;

        dx = abs(x2 - x1);
        dy = abs(y2 - y1);

        if (dx >= dy)
            step = dx;
        else
            step = dy;

        dx = dx / step;
        dy = dy / step;

        x = x1;
        y = y1;

        i = 1;
        while (i <= step){
            PutPixelColor(x, y, p);

            x = x + dx;
            y = y + dy;
            i = i + 1;
        }
    }

    void DrawRectangle(int x,int y,int width,int height, PixelColor p = PixelColor()) {
	
	    Drawline(x,y,x+ width,y,p); // top 
	    Drawline(x,y+ height,x+ width,y+ height,p); // bottom

        Drawline(x, y, x, y + height,p); // left
        Drawline(x + width, y, x + width, y + height,p); // right
    }

    void loadDefaultFont() {
        unsigned char* fontBuffer;

        auto fs = cmrc::LanaResources::get_filesystem();
        auto font = fs.open("res/OpenSans-VariableFont_wdth,wght.ttf");

        fontBuffer = (unsigned char*)malloc(font.size());
        std::memcpy(fontBuffer, font.begin(), font.size());

        fonts.push_back(fontInfo("OpenSans-Variable", fontBuffer, font.size()));
    }

    bool loadFont(std::string path) {
        unsigned char* fontBuffer;
        long fontBufferSize;
        
        FILE* fontFile = fopen(path.c_str(), "rb");

        if (fontFile == NULL) return false;

        fseek(fontFile, 0, SEEK_END);
        fontBufferSize = ftell(fontFile); /* how long is the file ? */
        fseek(fontFile, 0, SEEK_SET); /* reset */

        fontBuffer = (unsigned char*)malloc(fontBufferSize);

        fread(fontBuffer, fontBufferSize, 1, fontFile);
        fclose(fontFile);

        fonts.push_back(fontInfo(path, fontBuffer, fontBufferSize));

        return true;
    }

    void DrawText(int x,int y, std::wstring word) {
        loadDefaultFont();

        /* prepare font */
        stbtt_fontinfo info;
        if (!stbtt_InitFont(&info, fonts[0].fontBuffer, 0)) {
            printf("failed\n");
        }

        int b_w = 512; /* bitmap width */
        int b_h = 128; /* bitmap height */
        int l_h = 64; /* line height */

        /* create a bitmap for the phrase */
        unsigned char* bitmap = (unsigned char*)calloc(b_w * b_h, sizeof(unsigned char));
        //unsigned char* bitmap = (unsigned char* )malloc(b_w*b_h*sizeof(unsigned char));

        /* calculate font scaling */
        float scale = stbtt_ScaleForPixelHeight(&info, l_h);

        int X = 0;

        int ascent, descent, lineGap;
        stbtt_GetFontVMetrics(&info, &ascent, &descent, &lineGap);

        ascent = roundf(ascent * scale);
        descent = roundf(descent * scale);

        for (int i = 0; i < word.size(); ++i) {
            /* how wide is this character */
            int ax;
            int lsb;
            stbtt_GetCodepointHMetrics(&info, word[i], &ax, &lsb);
            /* (Note that each Codepoint call has an alternative Glyph version which caches the work required to lookup the character word[i].) */

            /* get bounding box for character (may be offset to account for chars that dip above or below the line */
            int c_x1, c_y1, c_x2, c_y2;
            stbtt_GetCodepointBitmapBox(&info, word[i], scale, scale, &c_x1, &c_y1, &c_x2, &c_y2);

            /* compute y (different characters have different heights */
            int y = ascent + c_y1;

            /* render character (stride and offset is important here) */
            int byteOffset = X + roundf(lsb * scale) + (y * b_w);
            stbtt_MakeCodepointBitmap(&info, bitmap + byteOffset, c_x2 - c_x1, c_y2 - c_y1, b_w, scale, scale, word[i]);


            /* advance X */
            X += roundf(ax * scale);

            /* add kerning */
            int kern;
            kern = stbtt_GetCodepointKernAdvance(&info, word[i], word[i + 1]);
            X += roundf(kern * scale);
        }

        //move text to main buffer (temporary solution) 
        for (int j = 0; j < b_h; j++) {
            for (int i = 0; i < b_w; i++) {
                auto currentBWpyxel = bitmap[j * b_w + i];
                
                if (currentBWpyxel != 0) {
                    PutPixelColor(i + x, j + y, PixelColor(currentBWpyxel, currentBWpyxel, currentBWpyxel, 255));
                }
            }
        }

        /*
         Note that this example writes each character directly into the target image buffer.
         The "right thing" to do for fonts that have overlapping characters is
         MakeCodepointBitmap to a temporary buffer and then alpha blend that onto the target image.
         See the stb_truetype.h header for more info.
        */
     
        delete bitmap;
    }

    void DeleteBuffer() {
        delete Buffer;
    }

};