set(LINUX_CXX_FLAGS "-no-pie")

if(NOT ${CMAKE_BUILD_TYPE} STREQUAL "Debug")
    string(APPEND LINUX_CXX_FLAGS " -s") # -s strip symbols
endif()

set(CMAKE_CXX_FLAGS ${LINUX_CXX_FLAGS})