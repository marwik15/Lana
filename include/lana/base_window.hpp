#pragma once
#include <string>

#ifdef _WIN32 //temporary 
#define DECLSPEC __declspec(dllexport)
#else
#define DECLSPEC 
#endif // _WIN32

namespace lana {

	struct Size {
		Size(){}
		Size(int w, int h) : w(w),h(h){}

		int w, h;
	};
	
	class DECLSPEC base_Window {
	public:
		struct WindowInfo{
			std::wstring name;
			Size size;
		};

		virtual void native_window_name(std::wstring) = 0;

		virtual int native_create() = 0;
		virtual void native_loop() = 0;
		virtual void native_show(bool) = 0;

		virtual void native_size(int, int) = 0;
		virtual lana::Size native_size() = 0;

		virtual void native_pos(int, int) = 0;
		virtual lana::Size native_pos() = 0;
	
	};

} //namespace lana