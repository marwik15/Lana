#pragma once

#ifdef linux

#include <lana/base_window.hpp>
#include <bufferDrawer/bufferDrawer.hpp>

#include <xcb/xcb.h>
#include <xcb/xcb_image.h>

#include <locale> 
#include <string>
#include <codecvt>

namespace lana {
  
    class xcbWindow : public base_Window {
    private:
        xcb_connection_t* c;
        xcb_screen_t* s;
        xcb_window_t w;
        xcb_pixmap_t pmap;
        xcb_gcontext_t gc;
        xcb_generic_event_t* e;
        uint32_t mask;
        uint32_t values[2];
        int done = 0;
        xcb_image_t* image;
        uint8_t* image32;
        xcb_expose_event_t* ee;
        std::string title = "";
        BufferDrawer bd;

    public:

        xcbWindow();

        int native_create() override;

        void native_window_name(std::wstring name) override;
     
        void native_loop() override;

        void native_show(bool) override;

        void native_size(int, int) override;
        lana::Size native_size() override {}

        void native_pos(int, int) override;
        lana::Size native_pos() override {}

        static xcb_format_t* find_format(xcb_connection_t* c, uint8_t depth, uint8_t bpp);

        xcb_image_t* CreateTrueColorImage(xcb_connection_t* c,int width, int height, void* data);
    };

} //namespace lana
#endif //linux