#pragma once

#ifdef _WIN32

#ifndef UNICODE
#define UNICODE
#endif 

#include <lana/base_window.hpp>
#include <Windows.h>
#include <gdiplus.h>
#pragma comment (lib,"Gdiplus.lib")

using namespace Gdiplus;
namespace lana {

    class __declspec(dllexport) win32Window : public base_Window {
    private:
        WindowInfo windowInfo;
        HWND hwnd;
    private:
    
        static LRESULT CALLBACK MessageRouter(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
            /// https://elcharolin.wordpress.com/2015/01/24/wndproc-as-a-class-member-win32/
            win32Window* app;
            app = (win32Window*)GetWindowLongPtr(hWnd, GWLP_USERDATA); //init
            return app->WindowProc(hWnd, msg, wParam, lParam);
        }

        LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

        void OnPaint(HDC hdc);
    
    public:

        win32Window();

        void native_size(int x,int y) override {
            SetWindowPos(hwnd, HWND_TOP, native_pos().w, native_pos().h, x, y, SWP_DRAWFRAME);
        }

        Size native_size() override {
            RECT lpRect;
            GetWindowRect(hwnd,&lpRect);
            return Size(lpRect.right, lpRect.bottom);
        }

        void native_pos(int x, int y) override{
            SetWindowPos(hwnd, HWND_TOP, x, y, native_size().w, native_size().h, SWP_DRAWFRAME);
        }

        Size native_pos() override {
            RECT lpRect;
            GetWindowRect(hwnd, &lpRect);
            return Size(lpRect.left, lpRect.top);

        }

        void native_loop() override {
            // Run the message loop.
            MSG msg = { };
            while (GetMessage(&msg, NULL, 0, 0)) {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }

        }

        void native_show(bool visible) override {
            ShowWindow(hwnd, visible);
            UpdateWindow(hwnd);
        }

        void native_window_name(std::wstring windowName) override {
            windowInfo.name = windowName;
            SetWindowTextW(hwnd, windowName.c_str());
        }

        int native_create() override {
            
            GdiplusStartupInput gdiplusStartupInput;
            ULONG_PTR           gdiplusToken;
            // Initialize GDI+.
            GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
            
            // Register the window class.
            const wchar_t CLASS_NAME[] = L"Lana Window Class";

            WNDCLASS wc = { };

            auto hInstance = GetModuleHandle(0);
            wc.lpfnWndProc = MessageRouter;
            wc.hInstance = hInstance;
            wc.lpszClassName = CLASS_NAME;
            wc.hCursor = LoadCursor(NULL,IDC_ARROW);

            RegisterClass(&wc);

            // Create the window.
            hwnd = CreateWindowEx(
                0,                              // Optional window styles.
                CLASS_NAME,                     // Window class
                L"Default Lana Window",    // Window text
                WS_OVERLAPPEDWINDOW,            // Window style

                // Size and position
                CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,

                NULL,       // Parent window    
                NULL,       // Menu
                hInstance,  // Instance handle
                NULL        // Additional application data
            );

            if (hwnd == NULL) {
                GdiplusShutdown(gdiplusToken);
                return 0;
            }
         
            return 0;
        }
    };
} //namespace lana
#endif //WIN32