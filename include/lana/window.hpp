#pragma once

#include <lana/native/win32_window.hpp>
#include <lana/native/xcb_window.hpp>

#ifdef linux
typedef lana::xcbWindow OSWINDOWCLASS;
#endif // linux

#ifdef _WIN32
typedef lana::win32Window OSWINDOWCLASS;
#endif // _WIN32

namespace lana {
	
	class DECLSPEC Window : private OSWINDOWCLASS {
	public:
		void create();
		void loop();
		void show(bool);
		void name(std::wstring);
		void name(std::string);

		void size(int, int);
		lana::Size size();

		void pos(int, int);
		lana::Size pos();

	};
} //namespace lana